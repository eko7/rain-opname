<?php

namespace Tests\Feature\Request;

use Tests\TestCase;

class StockopnameSubmitRequestTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->url = route('stockopname_submit.store');
    }

    /**
     * Test error message when field is not provided or empty.
     *
     * @return void
     */
    public function testRequired()
    {
        $form = [];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'username' => 'The username field is required.',
            'items' => 'The items field is required.',
        ]);
    }

    /**
     * Test error message when field is not an array.
     *
     * @return void
     */
    public function testArray()
    {
        $form = [
            'items' => 'some text',
        ];

        $this->jsonPost($form)->assertJsonValidationErrors([
            'items' => 'The items must be an array.',
        ]);
    }
}
