<?php

namespace Tests\Feature\Controller;

use Tests\TestCase;

class FileSystemControllerTest extends TestCase
{
    public function testDownload()
    {
        $this->url = route('file_system.download');
        $response = $this->jsonGet();
        $response->assertJson([
            [
                'command' => 'git pull origin main',
                'result' => "Already up to date.\n",
            ],
        ]);
    }
}
