<?php

namespace Tests\Feature\Controller;

use App\Models\StockopnameCompare;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StockopnameCompareControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    protected $url = '/stockopname_compare';
    public function testStockopnameCompareUpdate()
    {
        $compare = StockopnameCompare::factory()->create(['note' => 'Just Note']);
        $form = [
            'note' => 'Update Note',
        ];

        $this->jsonPut($form, $compare->id)->assertOk()->assertJson($expected = [
            'id' => $compare->id,
            'note' => $form['note'],
        ]);

        $this->assertDatabaseHas('stockopname_compares', $expected);
    }
}
