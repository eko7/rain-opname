<?php

namespace Tests\Feature\Controller;

use App\Models\Stockopname;
use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class StockopnameControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use RefreshDatabase;
    protected $submit_count = 3; //remove the sequence if you want to add more than 3 submits
    protected $detail_count = 10;
    protected $product_code = 'A0001*B*XL'; //need to insert this data manually on database first
    protected $username = [
        ['username' => 'Artic Uno'],
        ['username' => 'Zap Dos'],
        ['username' => 'Molten Tres'],
    ]; //made up username

    public function testStockopnameIndex()
    {
        $stockopname = Stockopname::factory()->create();
        StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->state(new Sequence(['username' => 'User']))
            ->has(
                StockopnameDetail::factory()
                    ->count($this->detail_count)
                    ->state(new Sequence(['product_code' => $this->product_code])),
                'detail'
            )
            ->create(['stockopname_id' => $stockopname->id]);

        //proses index, show, dan print stockopname
        $this->url = route('stockopname.index'); //index
        $this->jsonGet()->assertJson([
            'data' => [[
                'id' => $stockopname->id,
                'submit_count' => $this->submit_count,
                'detail_count' => $this->submit_count * $this->detail_count,
            ]],
            'meta' => [
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'per_page' => 9,
                'to' => 1,
                'total' => 1,
            ],
        ]); //result query table stockopnames, with count submit, with count detail
    }

    public function testStockopnameStore()
    {
        $submit = StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->state(new Sequence($this->username[0], $this->username[1], $this->username[2]))
            ->has(
                StockopnameDetail::factory()
                    ->count($this->detail_count)
                    ->state(new Sequence(['product_code' => $this->product_code])),
                'detail'
            )
            ->create();

        $this->url = route('stockopname.store');
        $this->jsonPost();

        //check stockopname, stockopname_submit, stockopname_detail
        $this->assertDatabaseCount('stockopnames', 1);
        for ($i = 0;$i < $this->submit_count;$i++) {
            $this->assertDatabaseHas('stockopname_submits', [
                'id' => $submit[$i]->id,
                'username' => $this->username[$i]['username'],
            ]);
            for ($j = 0;$j < $this->detail_count;$j++) {
                $this->assertDatabaseHas('stockopname_details', [
                    'stockopname_submit_id' => $submit[$i]->id,
                    'product_code' => $this->product_code,
                ]);
            }
        }
    }

    public function testStockopnameShow()
    {
        $stockopname = Stockopname::factory()->create();
        StockopnameSubmit::factory()
            ->count($this->submit_count)
            ->state(new Sequence($this->username[0], $this->username[1], $this->username[2]))
            ->has(
                StockopnameDetail::factory()
                    ->count($this->detail_count)
                    ->state(new Sequence(['product_code' => $this->product_code])),
                'detail'
            )
            ->create(['stockopname_id' => $stockopname->id]);

        $last_row = DB::table('stockopnames')->latest('created_at')->first();

        $this->url = route('stockopname.show', $last_row->id); //show
        $this->jsonGet()->assertJson([
            'data' => [
                ['username' => $this->username[0]['username'], 'detail_count' => $this->detail_count],
                ['username' => $this->username[1]['username'], 'detail_count' => $this->detail_count],
                ['username' => $this->username[2]['username'], 'detail_count' => $this->detail_count],
            ],
            'meta' => [
                'current_page' => 1,
                'from' => 1,
                'last_page' => 1,
                'per_page' => 9,
                'to' => 3,
                'total' => 3,
            ],
        ]); //result query table stockopname_submits, where id x, with count detail
    }

    public function testStockopnameDestroy()
    {
        //submit
        StockopnameSubmit::factory()
            ->count(3)
            ->hasDetail(10)
            ->create();

        $this->url = route('stockopname.destroy');
        $this->jsonPost();

        // check if data on submit and detail table has been ceompletely deleted
        $this->assertDatabaseCount('stockopname_details', 0);
        $this->assertDatabaseCount('stockopname_submits', 0);
    }
}
