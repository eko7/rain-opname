import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import components from './components'
import api from './functions/api'
import formatter from './functions/formatters'
const app = createApp(App)
app.config.globalProperties.api = api
app.config.globalProperties.fmt = formatter
app.use(router).use(components)

app.mount('#app')
