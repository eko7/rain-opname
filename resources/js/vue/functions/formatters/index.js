import { number } from './number'
import { dateShort, dateLong, dateNumbered, dateTime, time } from './date'
export default {
  number,
  dateLong,
  dateNumbered,
  dateShort,
  dateTime,
  time,
}