import numbro from 'numbro'
export const number = num => {
  num = num ? num : 0
  return numbro(num).format({ thousandSeparated: true })
}
export default {
  number,
}