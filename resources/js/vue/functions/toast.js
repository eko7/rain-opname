import { reactive } from 'vue'

export const toasts = reactive([])
export const toast = (message) => {
  toasts.push({ message, id: Date.now() })
  setTimeout(() => {
    toasts.shift()
  }, 2000)
}