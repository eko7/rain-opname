import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: () => import(/* webpackChunkName: "home" */ '../views/Dashboard'),
    children: [
      {
        path: '',
        name: 'home',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home'),
      },
      {
        path: 'detail/:id_stockopname_submit',
        name: 'detail',
        component: () => import(/* webpackChunkName: "detail" */ '../views/Detail'),
      },
      {
        path: 'riwayat',
        name: 'riwayat',
        component: () => import(/* webpackChunkName: "riwayat" */ '../views/Riwayat'),
      },
      {
        path: 'riwayat_detail/:id_stockopname',
        name: 'riwayat_detail',
        component: () => import(/* webpackChunkName: "riwayat" */ '../views/Riwayat/Detail'),
      },
      {
        path: 'laporan/:id_stockopname',
        name: 'laporan',
        component: () => import(/* webpackChunkName: "riwayat" */ '../views/Report'),
      },
    ],
  },
  {
    path: '/update',
    name: 'update',
    component: () => import(/* webpackChunkName: "update" */ '../views/Update'),
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

export default router