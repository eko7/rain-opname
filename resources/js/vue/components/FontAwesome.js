// https://www.npmjs.com/package/@fortawesome/vue-fontawesome

import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'

import {
  faChevronLeft,
  faCircle,
  faEllipsisV,
  faFileAlt,
  faFileUpload,
  faHistory,
  faHome,
  faPaperPlane,
  faPencilAlt,
  faRedo,
  faSave,
  faSearch,
  faSort,
  faSortDown,
  faSortUp,
  faSpinner,
  faSyncAlt,
  faTimes,
  faTimesCircle,
  faTrash,
  faUndo,
} from '@fortawesome/free-solid-svg-icons'
library.add(
  faChevronLeft,
  faCircle,
  faEllipsisV,
  faFileAlt,
  faFileUpload,
  faHistory,
  faHome,
  faPaperPlane,
  faPencilAlt,
  faRedo,
  faSave,
  faSearch,
  faSort,
  faSortDown,
  faSortUp,
  faSpinner,
  faSyncAlt,
  faTimes,
  faTimesCircle,
  faTrash,
  faUndo,
)

export default {
  install (Vue) {
    Vue.component('FaIcon', FontAwesomeIcon)
    Vue.component('FaLayers', FontAwesomeLayers)
    Vue.component('FaLayersText', FontAwesomeLayersText)
  },
}