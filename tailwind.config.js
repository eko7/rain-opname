module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.vue',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        blue: {
          '100': '#B8DCF2',
          '200': '#9ECFED',
          '300': '#83C2E8',
          '400': '#69B5E3',
          '500': '#4EA8DE',
          '600': '#428FBD',
          '700': '#37769B',
          '800': '#2B5C7A',
          '900': '#1F4359',
        },
        gray: {
          '100': '#B6B9BC',
          '200': '#9B9FA3',
          '300': '#808589',
          '400': '#646A70',
          '500': '#495057',
          '600': '#3E444A',
          '700': '#33383D',
          '800': '#282C30',
          '900': '#1D2023',
        },
        red: {
          '100': '#FFBDBF',
          '200': '#FFA4A7',
          '300': '#FF8C8F',
          '400': '#FF7377',
          '500': '#FF5A5F',
          '600': '#D94D51',
          '700': '#B33F43',
          '800': '#8C3234',
          '900': '#662426',
        },
        green: {
          '100': '#BAE2CF',
          '200': '#A0D7BE',
          '300': '#86CDAC',
          '400': '#6CC29A',
          '500': '#52B788',
          '600': '#469C74',
          '700': '#39805F',
          '800': '#2D654B',
          '900': '#214936',
        },
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['disabled'],
      ringWidth: ['focus-visible'],
      ringColor: ['focus-visible'],
    },
  },
  plugins: [],
}
