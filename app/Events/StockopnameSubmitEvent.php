<?php

namespace App\Events;

use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StockopnameSubmitEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $stockopname_submit;
    protected $items;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(StockopnameSubmit $stockopname_submit, $items)
    {
        $this->stockopname_submit = $stockopname_submit;
        $this->items = $items;
    }

    public function broadcastWith()
    {
        return [
            'id' => $this->stockopname_submit->id,
            'username' => $this->stockopname_submit->username,
            'items' => $this->items,
        ];
    }

    public function broadcastAs()
    {
        return 'newSubmit';
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('stockopname-channel-' . env('SHOP_NAME'));
    }
}
