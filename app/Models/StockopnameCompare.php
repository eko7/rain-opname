<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockopnameCompare extends Model
{
    use HasFactory;

    public static function getCategorizedStockopname($stockopname_compare, $state = 'web')
    {
        if ($state == 'web') {
            $category = [
                ['name' => 'less', 'title' => 'Kurang', 'color_head' => 'bg-red-500', 'color_row' => 'bg-red-50', 'colspan' => 6, 'sortedResult' => []],
                ['name' => 'more', 'title' => 'Lebih', 'color_head' => 'bg-green-500', 'color_row' => 'bg-green-50', 'colspan' => 6, 'sortedResult' => []],
                ['name' => 'missed', 'title' => 'Terlewat', 'color_head' => 'bg-yellow-500', 'color_row' => 'bg-yellow-50', 'colspan' => 4, 'sortedResult' => []],
                ['name' => 'new', 'title' => 'Tidak Terdaftar', 'color_head' => 'bg-blue-500', 'color_row' => 'bg-blue-50', 'colspan' => 4, 'sortedResult' => []],
            ];
        } else {
            $category = [
                ['class' => 'stockopname-less', 'category' => 'less', 'title' => 'Kurang', 'color_head' => '#FF5A5F', 'color_row' => '#ffe6e6', 'sortedResult' => []],
                ['class' => 'stockopname-more', 'category' => 'more', 'title' => 'Lebih', 'color_head' => '#52B788', 'color_row' => '#edf7f3', 'sortedResult' => []],
                ['class' => 'stockopname-missed', 'category' => 'missed', 'title' => 'Terlewat', 'color_head' => '#FFB30C', 'color_row' => '#fff7e6', 'sortedResult' => []],
                ['class' => 'stockopname-new', 'category' => 'new', 'title' => 'Tidak Terdaftar', 'color_head' => '#4EA8DE', 'color_row' => '#e9f4fb', 'sortedResult' => []],
            ];
        }

        foreach ($stockopname_compare as $data) {
            if ($data->stockopname > $data->stockdb) {
                if (!isset($data->stockdb)) {
                    $category[3]['sortedResult'][] = $data;
                } else {
                    $category[1]['sortedResult'][] = $data;
                }
            } elseif ($data->stockopname < $data->stockdb) {
                if ($data->stockopname == 0) {
                    $category[2]['sortedResult'][] = $data;
                } else {
                    $category[0]['sortedResult'][] = $data;
                }
            }
        }

        return $category;
    }
}
