<?php

namespace App\Http\Controllers;

use App\Models\ShopStock;
use App\Models\Stockopname;
use App\Models\StockopnameCompare;
use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockopnameCompareController extends Controller
{
    /**
     * Test StockopnameSubmitController@update
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StockopnameCompare  $request
     * @param  \App\Models\StockopnameCompare  $stockopnameCompare
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockopnameCompare $stockopnameCompare)
    {
        return DB::transaction(function () use ($request, $stockopnameCompare) {
            $validated = $request->validate([
                'oldProductCode' => 'nullable|string',
                'newProductCode' => 'nullable|string',
                'note' => 'nullable|string',
            ]);

            if (isset($validated['newProductCode'])) {
                StockopnameDetail::where('product_code', $validated['oldProductCode'])->update(['product_code' => strtoupper($validated['newProductCode'])]);
            } else {
                $stockopnameCompare->note = $validated['note'];
                $stockopnameCompare->save();
            }

            return $stockopnameCompare;
        });
    }

    public function createReport($stockopname_id, $state = 'view')
    {
        return DB::transaction(function () use ($stockopname_id, $state) {
            $stockopname_compare = StockopnameCompare::where('stockopname_id', $stockopname_id)->get();
            $compare_exist = count($stockopname_compare);
            $prev_compare_product_code = $compare_exist ? $stockopname_compare->pluck('product_code') : collect([]);
            if (!$compare_exist || $state == 'update') {
                $stockopname = Stockopname::find($stockopname_id);
                $chunk = 10000000;
                $shop_stock = new ShopStock();
                $details = StockopnameSubmit::where('stockopname_id', $stockopname_id)
                    ->leftjoin('stockopname_details', 'stockopname_submits.id', '=', 'stockopname_details.stockopname_submit_id')
                    ->select('stockopname_details.product_code as product_code', DB::raw('COUNT(product_code) as stock'))
                    ->groupBy('stockopname_details.product_code')
                    ->havingRaw('COUNT(stockopname_details.product_code) > ?', [0])
                    ->get();

                $detail_chunk = $details->chunk($chunk);
                $compares = collect([]);
                foreach ($detail_chunk as $detailchunk) {
                    $stockdb = ShopStock::select(DB::raw('upper(' . ShopStock::$COLUMN_PRODUCT_CODE . ') as ' . ShopStock::$COLUMN_PRODUCT_CODE), ShopStock::$COLUMN_STOCK)
                        ->whereIn(ShopStock::$COLUMN_PRODUCT_CODE, $detailchunk->pluck('product_code'))->get();
                    if (isset($stockdb)) {
                        $stockdb_product_code = $stockdb->pluck(ShopStock::$COLUMN_PRODUCT_CODE);
                        $comparing = $detailchunk->map(function ($detail) use ($stockdb, $stockopname_compare, $stockdb_product_code, $prev_compare_product_code) {
                            $index_product_code = $stockdb_product_code->search($detail->product_code, true);
                            $index_product_code_on_prev_compare = $prev_compare_product_code->search($detail->product_code, true);
                            $compare = new StockopnameCompare();
                            $compare->product_code = $detail->product_code;
                            $compare->stockopname = $detail->stock;
                            $compare->stockdb = is_bool($index_product_code) ? null : $stockdb[$index_product_code][ShopStock::$COLUMN_STOCK];
                            $compare->note = is_bool($index_product_code_on_prev_compare) ? '' : $stockopname_compare[$index_product_code_on_prev_compare]['note'];

                            return $compare;
                        });
                        $compares = $compares->merge($comparing);
                    }
                }

                $stockdb = ShopStock::whereNotIn(ShopStock::$COLUMN_PRODUCT_CODE, $detailchunk->pluck('product_code'))->where(ShopStock::$COLUMN_STOCK, '>', 0)->get();
                $comparing = $stockdb->map(function ($stock) use ($stockopname_compare, $prev_compare_product_code) {
                    $index_product_code_on_prev_compare = $prev_compare_product_code->search(strtoupper($stock[ShopStock::$COLUMN_PRODUCT_CODE]), true);
                    $compare = new StockopnameCompare();
                    $compare->product_code = strtoupper($stock[ShopStock::$COLUMN_PRODUCT_CODE]);
                    $compare->stockopname = 0;
                    $compare->stockdb = $stock[ShopStock::$COLUMN_STOCK];
                    $compare->note = is_bool($index_product_code_on_prev_compare) ? '' : $stockopname_compare[$index_product_code_on_prev_compare]['note'];

                    return $compare;
                });
                StockopnameCompare::where('stockopname_id', $stockopname_id)->delete();
                $compares = $compares->merge($comparing);
                $stockopname->compare()->saveMany($compares);
            }

            $stockopname = Stockopname::find($stockopname_id)->load('submit', 'compare');
            $category = StockopnameCompare::getCategorizedStockopname($stockopname->compare);

            return [
                'categories' => $category,
                'stockopname' => $stockopname,
            ];
        });
    }

    public function print(Request $request, $stockopname_id, $state = 'view')
    {
        $today = Carbon::now()->translatedFormat('l, d F Y');
        $stockopname = Stockopname::find($stockopname_id)->load('submit', 'compare');

        $category = StockopnameCompare::getCategorizedStockopname($stockopname->compare, 'print');

        foreach ($category as $key => $categorizedData) {
            $sort = explode(' ', $request[$categorizedData['category']]);
            if (count($categorizedData['sortedResult']) > 1) {
                switch ($sort[0]) {
                    case 'code':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return strnatcmp($a['product_code'], $b['product_code']);
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -strnatcmp($a['product_code'], $b['product_code']);
                            });
                        break;
                    case 'qty':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return $a['stockopname'] <=> $b['stockopname'];
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -($a['stockopname'] <=> $b['stockopname']);
                            });
                        break;
                    case 'stock':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return $a['stockdb'] <=> $b['stockdb'];
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -($a['stockdb'] <=> $b['stockdb']);
                            });
                        break;
                    case 'diff':
                        $sort[1] == 'up' ?
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return ($a['stockopname'] - $a['stockdb']) <=> ($b['stockopname'] - $b['stockdb']);
                            }) :
                            usort($categorizedData['sortedResult'], function ($a, $b) {
                                return -(($a['stockopname'] - $a['stockdb']) <=> ($b['stockopname'] - $b['stockdb']));
                            });
                        break;
                }
                $category[$key]['sortedResult'] = $categorizedData['sortedResult'];
            }
        }

        $pdf = app('dompdf.wrapper')->loadView('pdf.stockopname', [
            'date' => $stockopname->created_at->isoFormat('dddd, D MMMM Y'),
            'today' => $today,
            'category' => $category,
        ]);
        $pdf->setPaper('a4', 'portrait');
        $dom_pdf = $pdf->getDomPDF();
        $canvas = $dom_pdf->get_canvas();
        $x = 520; //x coordinate
        $y = 800; //y coordinate
        $text = 'Lembar {PAGE_NUM} / {PAGE_COUNT}';
        $text_font = null;
        $text_size = 10;
        $canvas->page_text($x, $y, $text, $text_font, $text_size, [0, 0, 0]);

        if ($state == 'view') {
            return view('pdf.stockopname', [
                'date' => $stockopname->created_at->isoFormat('dddd, D MMMM Y'),
                'today' => $today,
                'category' => $category,
            ]);
        } elseif ($state == 'viewPdf') {
            return $pdf->stream('laporan-stockopname-' . $stockopname->created_at->isoFormat('dddd, D MMMM Y') . '.pdf');
        } else {
            return $pdf->download('laporan-stockopname-' . $stockopname->created_at->isoFormat('dddd, D MMMM Y') . '.pdf');
        }
    }
}
