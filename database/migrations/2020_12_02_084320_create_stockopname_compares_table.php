<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockopnameComparesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockopname_compares', function (Blueprint $table) {
            $table->id();
            $table->foreignId('stockopname_id')->constrained();
            $table->string('product_code')->index();
            $table->integer('stockopname')->index();
            $table->integer('stockdb')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockopname_compares');
    }
}
