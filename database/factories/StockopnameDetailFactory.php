<?php

namespace Database\Factories;

use App\Models\StockopnameDetail;
use App\Models\StockopnameSubmit;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockopnameDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StockopnameDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {        
        return [
            'stockopname_submit_id' => StockopnameSubmit::factory(),
            'product_code' => 'A' . rand(1, 10000),
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
