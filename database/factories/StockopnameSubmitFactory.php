<?php

namespace Database\Factories;

use App\Models\StockopnameSubmit;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockopnameSubmitFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StockopnameSubmit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {        
        return [
            'username' => $this->faker->name,
            'created_at' => now(),
            'updated_at' => now()
        ];
    }
}
