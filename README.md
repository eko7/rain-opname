# Cara Install

- git clone https://gitlab.com/gradintech/rain-opname.git
- cp .env.example .env
- composer install
- composer require doctrine/dbal
- php artisan key:generate
- set env:
  - APP_ENV=local
  - SHOP_NAME=NAMA TOKO (tanya chris)
  - DB_DATABASE=stockopname
  - DB_USERNAME=rain
  - DB_PASSWORD=rainboutique.net
  - DB_DATABASE_SHOP=rainshopdb
  - DB_USERNAME_SHOP=rain
  - DB_PASSWORD_SHOP=rainboutique.net
  - DB_TABLE_ITEM=mstitem
  - DB_TABLE_ITEM_COLUMN_STOCK=stoktoko
  - DB_TABLE_ITEM_COLUMN_PRODUCT_CODE=kode
- npm install
- npm run production
- buka heidi user / pass: root / *******x5
- create new database stockopname
- klik tombol manage user (di sebelah reload)
- pilih user rain
- add object stockopname (bisa semua)
- add object rainshopdb (bisa select mstitem saja)
- pastikan bisa login rain / rainboutique.net buka database stockopname & rainshopdb table mstitem
- php artisan migrate
- kalau laragon, skip kalau xampp: 
  - jalankan Firewall Stockopname Port 1133.reg
  - buka laragon apache > sites enabled > 00-default.conf
  - copy paste apache config di bawah
- kalau xampp:
  - download laragon portable
  - download php 7.4, taruh di laragon/bin/php
  - download nodejs, taruh di laragon/bin/nodejs
  - laragon > preferences > services & ports > uncheck mysql
  - download git : tools > quick add > git
  - tambahkan path ke environment : tools > path > add laragon to path
- buka app nya, install to desktop
- testing pakai hp



## Apache Config

```
Listen 1133
<VirtualHost *:1133>
    DocumentRoot "D:/Sources/laragon/www/rain-opname/public/"
    <Directory "D:/Sources/laragon/www/rain-opname/public/">
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```